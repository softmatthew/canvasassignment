package com.example.matthew.canvasassigno1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Screenone extends AppCompatActivity {
    private Button screen1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screenone);
        screen1 = findViewById(R.id.button1);
        screen1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Screenone.this,  MainActivity.class);
                startActivity(intent);
                finish();

            }
        });
    }
}
